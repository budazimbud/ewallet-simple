import { db ,dbHistory} from "./database";
import { WalletTransaction } from "./model";
import { Wallet } from "./model";

export class UserRepository{

    static find(PhoneUserOrigin:string):Wallet{
       for(let i =0;i<db.length;i++){
           if(db[i].noTelpon==PhoneUserOrigin){
                return db[i];
           }
       }

       throw new Error('phone tidak di temukan')
   }

   static create(PhoneUserOrigin:string,amount:number):Wallet{
               for(let i =0;i<db.length;i++){
                       if(db[i].noTelpon==PhoneUserOrigin){
                            db[i].balance+=amount;
                            return db[i];
                       }
               }
       throw new Error(`maaf top anda gagal ${PhoneUserOrigin}`)
   }



}


export class historyTransactionRepository  {
 
   static createHistory(User:Wallet,typeTransfer:string,amount:number): void {
        const date = new Date();
        const history = new WalletTransaction();
        history.user = User;
        history.typeTransaction = typeTransfer
        history.noReference =  Math.random().toString(36).substr(2, 9);
        history.date = date.toString();
        history.amount = amount
        dbHistory.push(history);
   }
   static listHistory(): void {
   }

}
