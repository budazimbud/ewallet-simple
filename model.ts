export class Wallet{
    nama:string|undefined;
    balance:number=0;
    noTelpon:string|undefined;
}



export class WalletTransaction{
    user: Wallet;
    noReference: string|undefined;
    typeTransaction:string|undefined;
    date:string;
    amount:number;
}

export interface ITransactionWallet{
    transfer(userTransaction: UserTransactionCreate):void;
    topUp(userTransaction:UserTransactionCreate):void;
    getHistory(nomeberPhone:string):void;
 
}