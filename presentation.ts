import {ePay,TransactionWallet,UserAccount} from './bussineslogic'
import { db } from './database';
export class UserWalletCreate{
    phoneNumber:string='';
    deviceId:string='';

}

export class UserTransactionCreate{
    userOrigin: UserWalletCreate;
    userDestination:UserWalletCreate;
    amount:number=0;
    createDate:Date;    

}


const azim = new ePay();
azim.nama='azim';
azim.noTelpon='123';

const hendar = new ePay();
hendar.nama='hendar';
hendar.noTelpon='321';

const ramdani = new ePay();
ramdani.nama='ramdani';
ramdani.noTelpon='456';


const userAccount = new UserAccount();

userAccount.accountRegister(azim);
userAccount.accountRegister(hendar);
userAccount.accountRegister(ramdani);

const userTransaction = new UserTransactionCreate();


const userwalletOrigin = new UserWalletCreate();
const userwalletDestination = new UserWalletCreate();

const transaksiWallet = new TransactionWallet();

userwalletOrigin.phoneNumber = azim.noTelpon;//pengirim
userwalletOrigin.deviceId = '02';

userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;

transaksiWallet.topUp(userTransaction);

//ramdani topup sebesar 4000
userwalletOrigin.phoneNumber = ramdani.noTelpon;//pengirim
userwalletOrigin.deviceId = '01';

userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;

transaksiWallet.topUp(userTransaction);

//hendar topup sebesar 4000
userwalletOrigin.phoneNumber = hendar.noTelpon;//pengirim
userwalletOrigin.deviceId = '03';

userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;

transaksiWallet.topUp(userTransaction);


//transaksi ramdani ke azim sebesar 1000
userwalletOrigin.phoneNumber = ramdani.noTelpon;//pengirim
userwalletOrigin.deviceId = '01';
userwalletDestination.phoneNumber = azim.noTelpon;//penerima
userwalletDestination.deviceId = '02';

userTransaction.userOrigin = userwalletOrigin;
userTransaction.userDestination = userwalletDestination;
userTransaction.amount = 1000;//jumlah transaksi

transaksiWallet.transfer(userTransaction);

//transaksi azim ke hendar sebesar 5000
userwalletOrigin.phoneNumber = azim.noTelpon;//pengirim
userwalletOrigin.deviceId = '02';
userwalletDestination.phoneNumber = hendar.noTelpon;//penerima
userwalletDestination.deviceId = '03';

userTransaction.userOrigin = userwalletOrigin;
userTransaction.userDestination = userwalletDestination;
userTransaction.amount = 5000;

transaksiWallet.transfer(userTransaction);

//azim topup sebesar 5000
userwalletOrigin.phoneNumber = azim.noTelpon;//pengirim
userwalletOrigin.deviceId = '02';


userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 5000;
transaksiWallet.topUp(userTransaction);


console.log(db);
transaksiWallet.getHistory(ramdani.noTelpon);