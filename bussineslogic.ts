import { db ,dbHistory} from "./database";
import { ITransactionWallet, Wallet } from "./model";
import { UserTransactionCreate } from "./presentation";
import { UserRepository,historyTransactionRepository } from "./repository";
export class UserAccount {
           
     
    accountRegister(accountData:Wallet){

                var result = db.find(e=>e.noTelpon===accountData.noTelpon);

                if(result){
                    console.log(`tolong ganti nomer telpon anda  ${accountData.nama}`);
                }else{
                  
                    db.push(accountData);

                }           
    }
}
export class ePay implements Wallet{
    nama: string='';
    balance: number=0;
    noTelpon: string='';
}

export class TransactionWallet implements ITransactionWallet{
            
    transfer(userTransaction: UserTransactionCreate) {
        let userWalletOrigin:Wallet;
        let userWalletDestionation:Wallet;
        userWalletOrigin  = UserRepository.find(userTransaction.userOrigin.phoneNumber);
        userWalletDestionation  = UserRepository.find(userTransaction.userDestination.phoneNumber);

        this.credit(userWalletOrigin,userWalletDestionation,userTransaction.amount);
        this.debit(userWalletOrigin, userWalletDestionation, userTransaction.amount);
            
        
    }

    topUp(userTransaction:UserTransactionCreate): void {
        // console.log(
        let userWalletOrigin:Wallet;   
        userWalletOrigin = UserRepository.create(userTransaction.userOrigin.phoneNumber,userTransaction.amount);
        historyTransactionRepository.createHistory(userWalletOrigin,'topup',userTransaction.amount);
    }



  

    getHistory(nomerPhone:string): void {
            
        for(let i =0;i<dbHistory.length;i++){
            if(dbHistory[i].user.noTelpon==nomerPhone){
                console.log(dbHistory[i])
            }
        }



            
    }

    private debit(userWalletOrigin:Wallet, userWalletDestination:Wallet ,amount:number):number{
        // console.log(typeof userwalletOrigin.phoneNumber);
        if(userWalletOrigin.balance>=amount){
            for(let i =0;i<db.length;i++){
                    if(db[i].noTelpon==userWalletOrigin.noTelpon){
                        historyTransactionRepository.createHistory(userWalletOrigin,'debit',amount);
                        return db[i].balance-=amount;
                    }

                }
         }
        
        throw new Error(`maaf ${userWalletOrigin.noTelpon} saldo anda tidak cukup`)
          
    }

    private credit(userWalletOrigin:Wallet, userWalletDestination:Wallet ,amount:number):number{

        if(userWalletOrigin.balance>=amount){
            for(let i =0;i<db.length;i++){
                    if(db[i].noTelpon==userWalletDestination.noTelpon){
                        historyTransactionRepository.createHistory(userWalletDestination,'credit',amount);
                        return db[i].balance+=amount;
                    }

                }
        }
        
        throw new Error(`maaf ${userWalletOrigin.noTelpon} saldo anda tidak cukup`)  
        
    }





    
}


