"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WalletTransaction = exports.Wallet = void 0;
var Wallet = /** @class */ (function () {
    function Wallet() {
        this.balance = 0;
    }
    return Wallet;
}());
exports.Wallet = Wallet;
var WalletTransaction = /** @class */ (function () {
    function WalletTransaction() {
    }
    return WalletTransaction;
}());
exports.WalletTransaction = WalletTransaction;
