"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.historyTransactionRepository = exports.UserRepository = void 0;
var database_1 = require("./database");
var model_1 = require("./model");
var UserRepository = /** @class */ (function () {
    function UserRepository() {
    }
    UserRepository.find = function (PhoneUserOrigin) {
        for (var i = 0; i < database_1.db.length; i++) {
            if (database_1.db[i].noTelpon == PhoneUserOrigin) {
                return database_1.db[i];
            }
        }
        throw new Error('phone tidak di temukan');
    };
    UserRepository.create = function (PhoneUserOrigin, amount) {
        for (var i = 0; i < database_1.db.length; i++) {
            if (database_1.db[i].noTelpon == PhoneUserOrigin) {
                database_1.db[i].balance += amount;
                return database_1.db[i];
            }
        }
        throw new Error("maaf top anda gagal " + PhoneUserOrigin);
    };
    return UserRepository;
}());
exports.UserRepository = UserRepository;
var historyTransactionRepository = /** @class */ (function () {
    function historyTransactionRepository() {
    }
    historyTransactionRepository.createHistory = function (User, typeTransfer, amount) {
        var date = new Date();
        var history = new model_1.WalletTransaction();
        history.user = User;
        history.typeTransaction = typeTransfer;
        history.noReference = Math.random().toString(36).substr(2, 9);
        history.date = date.toString();
        history.amount = amount;
        database_1.dbHistory.push(history);
    };
    historyTransactionRepository.listHistory = function () {
    };
    return historyTransactionRepository;
}());
exports.historyTransactionRepository = historyTransactionRepository;
