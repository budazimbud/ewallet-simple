"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTransactionCreate = exports.UserWalletCreate = void 0;
var bussineslogic_1 = require("./bussineslogic");
var database_1 = require("./database");
var UserWalletCreate = /** @class */ (function () {
    function UserWalletCreate() {
        this.phoneNumber = '';
        this.deviceId = '';
    }
    return UserWalletCreate;
}());
exports.UserWalletCreate = UserWalletCreate;
var UserTransactionCreate = /** @class */ (function () {
    function UserTransactionCreate() {
        this.amount = 0;
    }
    return UserTransactionCreate;
}());
exports.UserTransactionCreate = UserTransactionCreate;
var azim = new bussineslogic_1.ePay();
azim.nama = 'azim';
azim.noTelpon = '123';
var hendar = new bussineslogic_1.ePay();
hendar.nama = 'hendar';
hendar.noTelpon = '321';
var ramdani = new bussineslogic_1.ePay();
ramdani.nama = 'ramdani';
ramdani.noTelpon = '456';
var userAccount = new bussineslogic_1.UserAccount();
userAccount.accountRegister(azim);
userAccount.accountRegister(hendar);
userAccount.accountRegister(ramdani);
var userTransaction = new UserTransactionCreate();
var userwalletOrigin = new UserWalletCreate();
var userwalletDestination = new UserWalletCreate();
var transaksiWallet = new bussineslogic_1.TransactionWallet();
userwalletOrigin.phoneNumber = azim.noTelpon; //pengirim
userwalletOrigin.deviceId = '02';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;
transaksiWallet.topUp(userTransaction);
//ramdani topup sebesar 4000
userwalletOrigin.phoneNumber = ramdani.noTelpon; //pengirim
userwalletOrigin.deviceId = '01';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;
transaksiWallet.topUp(userTransaction);
//hendar topup sebesar 4000
userwalletOrigin.phoneNumber = hendar.noTelpon; //pengirim
userwalletOrigin.deviceId = '03';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 4000;
transaksiWallet.topUp(userTransaction);
//transaksi ramdani ke azim sebesar 1000
userwalletOrigin.phoneNumber = ramdani.noTelpon; //pengirim
userwalletOrigin.deviceId = '01';
userwalletDestination.phoneNumber = azim.noTelpon; //penerima
userwalletDestination.deviceId = '02';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.userDestination = userwalletDestination;
userTransaction.amount = 1000; //jumlah transaksi
transaksiWallet.transfer(userTransaction);
//transaksi azim ke hendar sebesar 5000
userwalletOrigin.phoneNumber = azim.noTelpon; //pengirim
userwalletOrigin.deviceId = '02';
userwalletDestination.phoneNumber = hendar.noTelpon; //penerima
userwalletDestination.deviceId = '03';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.userDestination = userwalletDestination;
userTransaction.amount = 5000;
transaksiWallet.transfer(userTransaction);
//azim topup sebesar 5000
userwalletOrigin.phoneNumber = azim.noTelpon; //pengirim
userwalletOrigin.deviceId = '02';
userTransaction.userOrigin = userwalletOrigin;
userTransaction.amount = 5000;
transaksiWallet.topUp(userTransaction);
console.log(database_1.db);
transaksiWallet.getHistory(ramdani.noTelpon);
