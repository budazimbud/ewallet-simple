"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TransactionWallet = exports.ePay = exports.UserAccount = void 0;
var database_1 = require("./database");
var repository_1 = require("./repository");
var UserAccount = /** @class */ (function () {
    function UserAccount() {
    }
    UserAccount.prototype.accountRegister = function (accountData) {
        var result = database_1.db.find(function (e) { return e.noTelpon === accountData.noTelpon; });
        if (result) {
            console.log("tolong ganti nomer telpon anda  " + accountData.nama);
        }
        else {
            database_1.db.push(accountData);
        }
    };
    return UserAccount;
}());
exports.UserAccount = UserAccount;
var ePay = /** @class */ (function () {
    function ePay() {
        this.nama = '';
        this.balance = 0;
        this.noTelpon = '';
    }
    return ePay;
}());
exports.ePay = ePay;
var TransactionWallet = /** @class */ (function () {
    function TransactionWallet() {
    }
    TransactionWallet.prototype.transfer = function (userTransaction) {
        var userWalletOrigin;
        var userWalletDestionation;
        userWalletOrigin = repository_1.UserRepository.find(userTransaction.userOrigin.phoneNumber);
        userWalletDestionation = repository_1.UserRepository.find(userTransaction.userDestination.phoneNumber);
        this.credit(userWalletOrigin, userWalletDestionation, userTransaction.amount);
        this.debit(userWalletOrigin, userWalletDestionation, userTransaction.amount);
    };
    TransactionWallet.prototype.topUp = function (userTransaction) {
        // console.log(
        var userWalletOrigin;
        userWalletOrigin = repository_1.UserRepository.create(userTransaction.userOrigin.phoneNumber, userTransaction.amount);
        repository_1.historyTransactionRepository.createHistory(userWalletOrigin, 'topup', userTransaction.amount);
    };
    TransactionWallet.prototype.getHistory = function (nomerPhone) {
        for (var i = 0; i < database_1.dbHistory.length; i++) {
            if (database_1.dbHistory[i].user.noTelpon == nomerPhone) {
                console.log(database_1.dbHistory[i]);
            }
        }
    };
    TransactionWallet.prototype.debit = function (userWalletOrigin, userWalletDestination, amount) {
        // console.log(typeof userwalletOrigin.phoneNumber);
        if (userWalletOrigin.balance >= amount) {
            for (var i = 0; i < database_1.db.length; i++) {
                if (database_1.db[i].noTelpon == userWalletOrigin.noTelpon) {
                    repository_1.historyTransactionRepository.createHistory(userWalletOrigin, 'debit', amount);
                    return database_1.db[i].balance -= amount;
                }
            }
        }
        throw new Error("maaf " + userWalletOrigin.noTelpon + " saldo anda tidak cukup");
    };
    TransactionWallet.prototype.credit = function (userWalletOrigin, userWalletDestination, amount) {
        if (userWalletOrigin.balance >= amount) {
            for (var i = 0; i < database_1.db.length; i++) {
                if (database_1.db[i].noTelpon == userWalletDestination.noTelpon) {
                    repository_1.historyTransactionRepository.createHistory(userWalletDestination, 'credit', amount);
                    return database_1.db[i].balance += amount;
                }
            }
        }
        throw new Error("maaf " + userWalletOrigin.noTelpon + " saldo anda tidak cukup");
    };
    return TransactionWallet;
}());
exports.TransactionWallet = TransactionWallet;
